# README #

This is a sample web app showing how to use the [S3 proxy](http://www.aerobatic.com/docs/) for your Aerobatic hosted web app.

The web app can be viewed at [http://s3-proxy.aerobatic.io/](http://s3-proxy.aerobatic.io/)